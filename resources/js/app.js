(function (){
    'use strict';
    // get form elements
    var form_btn = document.querySelector('.staff-button');
    var form = document.querySelector('.staff-form');
    // add submit event
    form_btn.addEventListener('click',saveStaffMember);
    /**
     * creates instance of an object if data entered are valid
     * @param e
     */
    function saveStaffMember (e) {
        e.preventDefault();
        var error_holders = document.querySelectorAll('.error');
        for(var i=0; i < error_holders.length; i++ ) {
           error_holders[i].parentNode.classList.remove('invalid');
        }
        var name = form.elements.namedItem('name').value,
            surname = form.elements.namedItem('surname').value,
            workplace = form.elements.namedItem('workplace').value,
            phone_number = form.elements.namedItem('phone_number').value,
            age = form.elements.namedItem('age').value;

        var errors = Validator.validate(name, surname,phone_number,age);
        if(errors.length == 0) {
            var person =new PersonFactory(name, surname,workplace,phone_number,age);;
            person.toString();
        } else {
            showValidationMsg(errors);
        }

    }

    /**
     * @param errors
     */
    function showValidationMsg(errors) {
        var error_holder;
        for(var i=0; i< errors.length; i++) {
            error_holder = document.querySelector('#'+errors[i].key).parentNode;
            error_holder.querySelector('.error').innerHTML = errors[i].value;
            error_holder.classList.add('invalid');

        }
    }
})(PersonFactory, Validator);