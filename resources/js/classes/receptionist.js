(function(){
    'use strict';

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Receptionist = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Receptionist';
        }
        // call parent constructor
        Person.call(this,name, surname, phone, age);
        this.guests = [];
    };
    /**
     * @param guest
     */
    Receptionist.prototype.addGuest = function (guest) {
        this.guests.push(guest);
    };
    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Receptionist}
     */
    function setInstance(name, surname, phone, age){
        return new Receptionist(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'reception';
    }
    // make Receptionist function global
    window.Receptionist = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);