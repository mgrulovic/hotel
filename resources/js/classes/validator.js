(function(){
    'use strict';
    /**
     * @param first_name
     * @param last_name
     * @param phone_number
     * @param person_age
     * @constructor
     */
    var Validator = function (first_name, last_name,phone_number, person_age) {
        /**
         * @param validate
         * @returns {boolean}
         */
        this.hasNumber = function(validate) {
            return /\d/.test(validate);
        };

        /**
         *
         * @param first_name
         * @param last_name
         * @param phone_number
         * @param person_age
         * @returns {Array}
         */
        this.validate = function(first_name, last_name,phone_number, person_age) {
            var errors = [];
            if(this.hasNumber(first_name) || first_name.length == 0) {
                errors.push({key:'name', value: 'Name is required and can\'t have numbers.'});;
            }
            if(this.hasNumber(last_name) || last_name.length == 0) {
                errors.push({key:'surname', value: 'Last name is required and can\'t have numbers.'});
            }
            if(phone_number.length == 0) {
                errors.push({key:'phone_number',value: 'Phone number is required.'});
            }

            if(!/^\d+$/.test(person_age) || person_age<18 || person_age > 60) {
                errors.push({key:'age', value: 'Age must be a number betwean 18 and 60.'});
            }
            return errors;

        };
         return this.validate(first_name, last_name,phone_number, person_age);
    };

    // make validator global
    window.Validator = {
        validate: Validator
    }
})();