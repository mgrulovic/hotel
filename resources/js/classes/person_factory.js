(function(){
    'use strict';
    var objects = arguments;

    /**
     * returns object based on a workplace
     * @param name
     * @param surname
     * @param workplace
     * @param phone
     * @param age
     * @returns {boolean}
     * @constructor
     */
    var PersonFactory = function(name, surname, workplace, phone, age) {
        var factory_object = false;
        for(var i =0; i<objects.length; i++) {
            if(objects[i].getWorkplace() == workplace) {
                factory_object = objects[i].setInstance(name, surname, phone, age);
                break;
            }
        }
        return factory_object;
    };

    window.PersonFactory = PersonFactory;
})(Manager, Maid, Cook, Receptionist, Barmen);