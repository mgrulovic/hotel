(function(){
    'use strict';

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Barmen = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Barmen';
        }
        Person.call(this,name, surname, phone, age);
        this.coctail_list = [];
    };

    /**
     * @param coctail
     */
    Barmen.prototype.addCoctails = function (coctail) {
        this.coctail_list.push(coctail);
    };

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Barmen}
     */
    function setInstance(name, surname, phone, age){
        return new Barmen(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'bar';
    }
    // make barmen functions global
    window.Barmen = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);