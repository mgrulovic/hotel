(function(){
    'use strict';
    /**
     *
     * @param name
     * @param surname
     * @param workplace
     * @param phone
     * @param age
     * @constructor
     */
    var Person = function(name, surname, phone, age) {

        this.first_name = name;
        this.last_name = surname;
        this.phone_number   = phone;
        this.person_age   = age;

        this.toString = function () {
            console.log(this.constructor.name +':'+ JSON.stringify( this));
        };
    };
    // make Person as global variable
    window.Person = Person;

})();