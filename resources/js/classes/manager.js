(function() {
    'use strict';
    /**
     * @param name
     * @param surname
     * @param workplace
     * @param phone
     * @param age
     * @constructor
     */
    var Manager = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Manager';
        }

        // call parent class constructor
        Person.call(this,name, surname, phone, age);
        // add manager specific attributes
        this.role = 'ADMIN';
        this.work_hours = '08-15h';
        this.workplace = 'office';


    };

    /**
     * instantiate manager
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Manager}
     */
    function setInstance(name, surname, phone, age){
        return new Manager(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'office';
    }
    // make manager functions global
    window.Manager = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);