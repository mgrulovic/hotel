(function(){
    'use strict';

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Cook = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Cook';
        }
        Person.call(this,name, surname, phone, age);
        this.recipes_ist = [];
    };
    /**
     * @param recipe
     */
    Cook.prototype.addRecipe = function (recipe) {
        this.recipes_ist.push(recipe
        );
    };

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Cook}
     */
    function setInstance(name, surname, phone, age){
        return new Cook(name, surname, phone, age);
    }

    /**
     * retruns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'kitchen';
    }
    // make cook functions global
    window.Cook = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);