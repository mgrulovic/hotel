(function(){
    'use strict';
    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Maid = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Maid';
        }
        // call parent constructor
        Person.call(this,name, surname, phone, age);
        this.rooms_to_clean = [];
    };
    /**
     * @param room_number
     */
    Maid.prototype.addRoomsToClean = function (room_number) {
      this.rooms_to_clean.push(room_number);
    };

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Maid}
     */
    function setInstance(name, surname, phone, age){
        return new Maid(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'rooms';
    }
    // make maid funstions global
    window.Maid = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);