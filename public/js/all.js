(function(){
    'use strict';
    /**
     *
     * @param name
     * @param surname
     * @param workplace
     * @param phone
     * @param age
     * @constructor
     */
    var Person = function(name, surname, phone, age) {

        this.first_name = name;
        this.last_name = surname;
        this.phone_number   = phone;
        this.person_age   = age;

        this.toString = function () {
            console.log(this.constructor.name +':'+ JSON.stringify( this));
        };
    };
    // make Person as global variable
    window.Person = Person;

})();
(function() {
    'use strict';
    /**
     * @param name
     * @param surname
     * @param workplace
     * @param phone
     * @param age
     * @constructor
     */
    var Manager = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Manager';
        }

        // call parent class constructor
        Person.call(this,name, surname, phone, age);
        // add manager specific attributes
        this.role = 'ADMIN';
        this.work_hours = '08-15h';
        this.workplace = 'office';


    };

    /**
     * instantiate manager
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Manager}
     */
    function setInstance(name, surname, phone, age){
        return new Manager(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'office';
    }
    // make manager functions global
    window.Manager = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);
(function(){
    'use strict';

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Cook = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Cook';
        }
        Person.call(this,name, surname, phone, age);
        this.recipes_ist = [];
    };
    /**
     * @param recipe
     */
    Cook.prototype.addRecipe = function (recipe) {
        this.recipes_ist.push(recipe
        );
    };

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Cook}
     */
    function setInstance(name, surname, phone, age){
        return new Cook(name, surname, phone, age);
    }

    /**
     * retruns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'kitchen';
    }
    // make cook functions global
    window.Cook = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);
(function(){
    'use strict';

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Barmen = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Barmen';
        }
        Person.call(this,name, surname, phone, age);
        this.coctail_list = [];
    };

    /**
     * @param coctail
     */
    Barmen.prototype.addCoctails = function (coctail) {
        this.coctail_list.push(coctail);
    };

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Barmen}
     */
    function setInstance(name, surname, phone, age){
        return new Barmen(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'bar';
    }
    // make barmen functions global
    window.Barmen = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);
(function(){
    'use strict';

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Receptionist = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Receptionist';
        }
        // call parent constructor
        Person.call(this,name, surname, phone, age);
        this.guests = [];
    };
    /**
     * @param guest
     */
    Receptionist.prototype.addGuest = function (guest) {
        this.guests.push(guest);
    };
    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Receptionist}
     */
    function setInstance(name, surname, phone, age){
        return new Receptionist(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'reception';
    }
    // make Receptionist function global
    window.Receptionist = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);
(function(){
    'use strict';
    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @constructor
     */
    var Maid = function(name, surname, phone, age) {
        // ie fix
        if(this.constructor.name == undefined) {
            this.constructor.name = 'Maid';
        }
        // call parent constructor
        Person.call(this,name, surname, phone, age);
        this.rooms_to_clean = [];
    };
    /**
     * @param room_number
     */
    Maid.prototype.addRoomsToClean = function (room_number) {
      this.rooms_to_clean.push(room_number);
    };

    /**
     * @param name
     * @param surname
     * @param phone
     * @param age
     * @returns {Maid}
     */
    function setInstance(name, surname, phone, age){
        return new Maid(name, surname, phone, age);
    }

    /**
     * returns static workplace
     * @returns {string}
     */
    function getWorkplace() {
        return 'rooms';
    }
    // make maid funstions global
    window.Maid = {
        setInstance: setInstance,
        getWorkplace: getWorkplace
    };
})(Person);
(function(){
    'use strict';
    var objects = arguments;

    /**
     * returns object based on a workplace
     * @param name
     * @param surname
     * @param workplace
     * @param phone
     * @param age
     * @returns {boolean}
     * @constructor
     */
    var PersonFactory = function(name, surname, workplace, phone, age) {
        var factory_object = false;
        for(var i =0; i<objects.length; i++) {
            if(objects[i].getWorkplace() == workplace) {
                factory_object = objects[i].setInstance(name, surname, phone, age);
                break;
            }
        }
        return factory_object;
    };

    window.PersonFactory = PersonFactory;
})(Manager, Maid, Cook, Receptionist, Barmen);
(function(){
    'use strict';
    /**
     * @param first_name
     * @param last_name
     * @param phone_number
     * @param person_age
     * @constructor
     */
    var Validator = function (first_name, last_name,phone_number, person_age) {
        /**
         * @param validate
         * @returns {boolean}
         */
        this.hasNumber = function(validate) {
            return /\d/.test(validate);
        };

        /**
         *
         * @param first_name
         * @param last_name
         * @param phone_number
         * @param person_age
         * @returns {Array}
         */
        this.validate = function(first_name, last_name,phone_number, person_age) {
            var errors = [];
            if(this.hasNumber(first_name) || first_name.length == 0) {
                errors.push({key:'name', value: 'Name is required and can\'t have numbers.'});;
            }
            if(this.hasNumber(last_name) || last_name.length == 0) {
                errors.push({key:'surname', value: 'Last name is required and can\'t have numbers.'});
            }
            if(phone_number.length == 0) {
                errors.push({key:'phone_number',value: 'Phone number is required.'});
            }

            if(!/^\d+$/.test(person_age) || person_age<18 || person_age > 60) {
                errors.push({key:'age', value: 'Age must be a number betwean 18 and 60.'});
            }
            return errors;

        };
         return this.validate(first_name, last_name,phone_number, person_age);
    };

    // make validator global
    window.Validator = {
        validate: Validator
    }
})();
(function (){
    'use strict';
    // get form elements
    var form_btn = document.querySelector('.staff-button');
    var form = document.querySelector('.staff-form');
    // add submit event
    form_btn.addEventListener('click',saveStaffMember);
    /**
     * creates instance of an object if data entered are valid
     * @param e
     */
    function saveStaffMember (e) {
        e.preventDefault();
        var error_holders = document.querySelectorAll('.error');
        for(var i=0; i < error_holders.length; i++ ) {
           error_holders[i].parentNode.classList.remove('invalid');
        }
        var name = form.elements.namedItem('name').value,
            surname = form.elements.namedItem('surname').value,
            workplace = form.elements.namedItem('workplace').value,
            phone_number = form.elements.namedItem('phone_number').value,
            age = form.elements.namedItem('age').value;

        var errors = Validator.validate(name, surname,phone_number,age);
        if(errors.length == 0) {
            var person =new PersonFactory(name, surname,workplace,phone_number,age);;
            person.toString();
        } else {
            showValidationMsg(errors);
        }

    }

    /**
     * @param errors
     */
    function showValidationMsg(errors) {
        var error_holder;
        for(var i=0; i< errors.length; i++) {
            error_holder = document.querySelector('#'+errors[i].key).parentNode;
            error_holder.querySelector('.error').innerHTML = errors[i].value;
            error_holder.classList.add('invalid');

        }
    }
})(PersonFactory, Validator);